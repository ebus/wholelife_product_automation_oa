package com.qa.testcases;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.AcceleratedQualifiersPageAction;
import com.qa.pageActions.ApplyESignatureSubmitAction;
import com.qa.pageActions.BeneficiaryInfoPageAction;
import com.qa.pageActions.CaseInformationPageAction;
import com.qa.pageActions.ChildrenBenefitRiderAction;
import com.qa.pageActions.ExistingInsuranceAction;
import com.qa.pageActions.FamilyHistoryCPPageAction;
import com.qa.pageActions.FamilyHistoryPageAction;
import com.qa.pageActions.FamilyHistorySPOPageAction;
import com.qa.pageActions.HIVConsentAction;
import com.qa.pageActions.HealthQuestionsChildrenPageAction;
import com.qa.pageActions.HealthQuestionsContPageAction;
import com.qa.pageActions.HealthQuestionsPageAction;
import com.qa.pageActions.HealthQuestionsSPOPageAction;
import com.qa.pageActions.HealthQuestionscoveredpersonPageAction;
import com.qa.pageActions.Illustration_PolicyPageAction;
import com.qa.pageActions.Illustration_RidersPageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.pageActions.MultipleOwnersPageAction;
import com.qa.pageActions.OwnerInfoContJuvenilePageAction;
import com.qa.pageActions.OwnerInfoContPageAction;
import com.qa.pageActions.OwnerInformationJuvenilePageAction;
import com.qa.pageActions.OwnerInformationPageAction;
import com.qa.pageActions.PerInfoSurviPurchaseOptPageAction;
import com.qa.pageActions.PerInfocoveredpersonPageAction;
import com.qa.pageActions.PersonalInformationAction;
import com.qa.pageActions.PremiumInformationAction;
import com.qa.pageActions.PremiumPayorInfoPageAction;
import com.qa.pageActions.ProposedInsuredInfoAction;
import com.qa.pageActions.RepresentativeInformationAction;
import com.qa.pageActions.SignatureMethodAction;
import com.qa.pageActions.SurvivorPurchaseOptionPageAction;
import com.qa.pageActions.TemporaryInsuranceAgreementAction;
import com.qa.pageActions.TermConversionPageAction;
import com.qa.pageActions.ThirdPartyOptionPageAction;
import com.qa.pageActions.USAPatriotActAction;
import com.qa.pageActions.ValidateAndLockDataAction;
import com.qa.pageActions.WholeLifeCoverageRidersAction;
import com.qa.pageActions.eSigDisclosuresAction;
import com.qa.pageActions.eSignatureConsentAction;
import com.qa.pageActions.eSignaturePartiesAction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class IllustrationTest extends TestBase {

	LoginPageAction loginPageAction;
	CaseInformationPageAction caseInformationPageAction;
	ProposedInsuredInfoAction proposedInsuredInfoAction;
	Illustration_PolicyPageAction illustration_PolicyPageAction;
	Illustration_RidersPageAction illustration_RidersPageAction;
	OwnerInformationPageAction ownerInformationPageAction;
	OwnerInformationJuvenilePageAction ownerInfoJuvenilePageAction;
	OwnerInfoContPageAction ownerInfoContPageAction;
	OwnerInfoContJuvenilePageAction ownerInfoContJuvenilePageAction;
	MultipleOwnersPageAction multipleOwnersPageAction;
	PremiumPayorInfoPageAction premiumPayorInfoPageAction;
	BeneficiaryInfoPageAction beneficiaryInfoPageAction;
	TermConversionPageAction termConversionPageAction;
	WholeLifeCoverageRidersAction wholeLifeCoverageRidersAction;
	AcceleratedQualifiersPageAction acceleratedQualifiersPageAction;
	ChildrenBenefitRiderAction childrenBenefitRiderAction;
	SurvivorPurchaseOptionPageAction survivorPurchaseOptionPageAction;
	PersonalInformationAction personalInformationAction;
	PerInfoSurviPurchaseOptPageAction perInfoSurviPurchaseOptPageAction;
	PerInfocoveredpersonPageAction perInfocoveredpersonPageAction;
	HealthQuestionsPageAction healthQuestionsPageAction;
	HealthQuestionsContPageAction healthQueContPageAction;
	HealthQuestionsChildrenPageAction healthQuestionsChildrenPageAction;
	HealthQuestionsSPOPageAction healthQuestionsSPOPageAction;
	HealthQuestionscoveredpersonPageAction healthQuestionscoveredpersonPageAction;
	FamilyHistoryPageAction familyHistoryPageAction;
	FamilyHistorySPOPageAction familyHistorySPOPageAction; 
	FamilyHistoryCPPageAction familyHistoryCPPageAction;
	HIVConsentAction HIVconsentAction;
	ExistingInsuranceAction existingInsuranceAction;
	PremiumInformationAction premiumInformationAction;
	USAPatriotActAction usaPatriotActAction;
	TemporaryInsuranceAgreementAction temporaryInsuranceAgreementAction;
	ThirdPartyOptionPageAction thirdPartyOptionPageAction;
	ValidateAndLockDataAction validateAndLockDataAction;
	SignatureMethodAction signatureMethodAction;
	eSigDisclosuresAction eSigdiclosuresAction;
	eSignatureConsentAction eSignatureconsentAction;
	eSignaturePartiesAction eSignaturepartiesAction;
	RepresentativeInformationAction representativeInformationAction;
	ApplyESignatureSubmitAction applyESignatureSubmitAction;
	
	int iteration = 0;

	public IllustrationTest() {
		super();
	}

	@DataProvider
	public Object[][] getApplicationData() {
		Object data[][] = TestUtil.getTestData("Application");
		return data;
	}



	@Test(priority = 1, dataProvider = "getApplicationData")
	public void IllustrationTest(String scenarioName, String indExecute, String userId, String pwd, String firstName, 
			String middleName, String lastName,String dob, String gender, String state, String productType,
			String product, String illustration, String isTermConversion, String child_firstname,String child_lastname,String child_relationship,String child_gender,String child_height_ft,String child_height_in,String child_weight,String child_dob,String child_all_listed,
			String doFaceAmtIncrease, String SSN, String birthCountry, 	
			String birthState, String maritalQuestion, String Street, String City, String ZipCode, 
			String County, String yearsAtAddress, String PhoneNumber, String willTheProposedInsuredBeOwner, String whoWillBePayor, 
			
			String payorRelationship, String payorEntityName,
			String payorFirstName, String payorLastName, String payorStreet, String payorCity, String payorState,
			String payorZIP, String payorCountry, String payorYrsAtAdd, String payorSSN, String payorDOB, 
			String payorBirthState,
			
			String applicationSignedBy, String indLegalGuardianPolicyOwner, String firstName_LG, String lastName_LG,
			String SSN_LG,
			
			String DoesInsuredHaveDL, String DLNo, String IssueState, String ExpirationDate, String earned,
			String unEarned, String netWorth, String isProposedInsuredUSCitizen, String countryofCitizenship, String doesProposedInsuredHoldGreenCard,	
			String greenCardNumber, String GCExpirationDate, String doesProposedInsuredHoldUSVisa, String typeOfVisa, String visaExpirationDate, 	
			String visaNumber, String provideDetails, String typeOwner, String relnProposedIns, String ownerFirstName, 
			String ownerLastName, String ownerSSN, String strDOB, String ownerBirthState, String ownerGender,
			
			
			
			String ownerMaritalStatus, String ownerDL_State, String strDLExpDate, String isMultipleOwner, String mulOwnerFirst,
			String mulOwnerLast,  String mOwner_Street, String mOwner_city, String mOwner_State, String mOwner_ZIPCode,
			String mOwner_DOB, String mOwner_SSN,  String numOfPB,
			String strShareIndicator, String strDeceasedSharePaid, String strRelationshipBene1, String strFirstNameBene1, String strLastNameBene1,
			String strGenderBene1, String strSSNBene1, String strSharePerBene1, String strEntityNameBene1, String strCorporateOfficer_Bene1, 
			String strTitle_Bene1, String strStateIncorporation_Bene1, String strOtherRelation_Bene1, String strRelationshipBene2, String strFirstNameBene2, 
			String strLastNameBene2, String strGenderBene2, String strSSNBene2, String strSharePerBene2, String strEntityNameBene2, 
			
			
			String strOtherRelation_Bene2, String strRelationshipBene3, String strFirstNameBene3, String strLastNameBene3, String strGenderBene3,
			String strSSNBene3, String strSharePerBene3, String strEntityNameBene3, String strOtherRelation_Bene3, String strRelationshipBene4, 
			String strFirstNameBene4, String strLastNameBene4, String strGenderBene4, String strSSNBene4, String strSharePerBene4, 
			String strEntityNameBene4, String strOtherRelation_Bene4, String strRelationshipBene5, String strFirstNameBene5, String strLastNameBene5,
			String strGenderBene5, String strSSNBene5, String strSharePerBene5, String strEntityNameBene5, String strOtherRelation_Bene5, 
			
			String limitedPayPeriod,
			String baseFaceAmount, String initialBasePremium, String cashWithApp, String premiumAmount, String dividendOption,
			String otherDividendOpt, String indexedDividendAllocation,
			String nonforfeiture, String paymentMode, String otherMode, String is_WPD,String isAcceleratorPaidRider, String isBlendAcceleratorRider, String billedpremium,String premium1035,String premium_non1035,String is_EBIR,String targetFaceAmt,String annualpremium,
			String isSameInsuredTermRider,String sitr_amount,String sitr_GuaranteedPeriod,String waiver_conversion_option,String is_cbr, String cbr_unit,String is_cpuad,String cpuad_amount,String isSurvivorPurchaseOptionRider,String SPO_amount,String SPO_first,String SPO_last, String isAUW, String tobacco_Uses,
			String isCompletingHealthInformation, String isExistingInsurance, String isPolicyReplacing, 
			String is1035Exchange, String isAdditionalRepresentatives, String AddRepresentativesCount, String representativesName1,
			String sharePercentage1, String representativesCode1, String representativesName2, String sharePercentage2, String representativesCode2,
			String representativesName3, String sharePercentage3, String representativesCode3, String representativesName4, String sharePercentage4, 
			String representativesCode4) {

		
		
		extentTest = extent.startTest("IllustrationTest - " + scenarioName);
		if (indExecute.equalsIgnoreCase("No")) {
			extentTest.log(LogStatus.INFO, "Execute column is No in the data sheet");
			throw new SkipException(scenarioName + " is Skipped");
		}
		
		if (illustration.equalsIgnoreCase("No")) {
			extentTest.log(LogStatus.INFO, "Illustration is selected as No in the data sheet");
			throw new SkipException(scenarioName + " is Skipped");		
		}
		
		
		Map<String,String> m = new HashMap();
		String[] arr = {"scn", "useUnisexRate","primaryInsuredClass","permanentFlatExtra","temporaryFlatExtra",
				"forYears", "inputMethods", "intialBaseFaceAmount", "premiumAmount", "dividends",
				"dividendOptionChange", "percentageDividendScale", "scheduleLoansWithdrawals",
				"withdrawalOptions", "loanInterest", "paymentMode", "waiverPremiumDisability", "coveredPerson", "firstName_CP",
				"lastName_CP", "dob_CP", "gender_CP", "classCP", "durationCP", "IsChildBenefitRider","CBR_unit","CBR_Duration","isPUARider",
				"1035SinglePremium", "monthReceived", "strNon1035SinglePremium", "isEnhancedBlendedInsuranceRider",
				"EBIRSpecifiedFace", "EBIRAmount", "targetPersistencyAge", "duration_EnhancedBlendedInsRider",
				"isSameInsuredTermRider", "plan_SameInsTermRider", "class_SameInsTermRider", "isSurvivorPurchaseOption",
				"firstName_SurvivorPurchaseOption", "lastName_SurvivorPurchaseOption", "gender_SurvivorPurchaseOption",
				"class_SurvivorPurchaseOption", "coverageAmt_SurvivorPurchaseOption", "duration_SurvivorPurchaseOption"};
		Object data2[][] = TestUtil.getTestData("Illustration");
		for(int i=0; i< data2[iteration].length; i++) {
			m.put(arr[i], (String) data2[iteration][i]);
		}
		iteration += 1;
			
			try {
				Thread.sleep(3000);
				
				intialization();

				extentTest.log(LogStatus.INFO, "Browser used: " + prop.getProperty("browser"));
				extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
				
				loginPageAction = new LoginPageAction();
				caseInformationPageAction = loginPageAction.logIn(userId, pwd);
				illustration_PolicyPageAction = caseInformationPageAction.selectProductIllustration(firstName, lastName, dob, gender,
						state, productType, product, illustration);
				illustration_RidersPageAction = illustration_PolicyPageAction.enterDataPolicyPage(gender, m.get("useUnisexRate"), 
						m.get("primaryInsuredClass"), m.get("permanentFlatExtra"), m.get("temporaryFlatExtra"), 
						m.get("forYears"), m.get("inputMethods"), m.get("intialBaseFaceAmount"), m.get("premiumAmount"),
						m.get("dividends"), m.get("dividendOptionChange"), m.get("percentageDividendScale"), 
						m.get("scheduleLoansWithdrawals"), m.get("withdrawalOptions"), m.get("loanInterest"), 
						m.get("paymentMode"), isTermConversion);
				proposedInsuredInfoAction = illustration_RidersPageAction.enterDataRiderPage(m.get("waiverPremiumDisability"), m.get("coveredPerson"), 
						m.get("firstName_CP"), m.get("lastName_CP"), m.get("dob_CP"),m.get("gender_CP"),
						m.get("classCP"), m.get("durationCP"),m.get("IsChildBenefitRider"),m.get("CBR_unit"),m.get("CBR_Duration"), m.get("isPUARider"), m.get("str1035SinglePremium"),
						m.get("monthReceived"), m.get("strNon1035SinglePremium"),m.get("isEnhancedBlendedInsuranceRider"),
						m.get("EBIRSpecifiedFace"), m.get("EBIRAmount"), m.get("targetPersistencyAge"),
						m.get("duration_EnhancedBlendedInsRider"), m.get("isSameInsuredTermRider"),
						m.get("plan_SameInsTermRider"), m.get("class_SameInsTermRider"),m.get("isSurvivorPurchaseOption"),
						m.get("firstName_SurvivorPurchaseOption"), m.get("lastName_SurvivorPurchaseOption"),
						m.get("gender_SurvivorPurchaseOption"), m.get("class_SurvivorPurchaseOption"),
						m.get("coverageAmt_SurvivorPurchaseOption"), m.get("duration_SurvivorPurchaseOption"));
				
				ownerInformationPageAction = proposedInsuredInfoAction.enterData(illustration, strStateIncorporation_Bene1, 
						isTermConversion, doFaceAmtIncrease, middleName, SSN, gender, birthCountry, birthState, 
						maritalQuestion, Street, City, ZipCode, County, yearsAtAddress, PhoneNumber, 
						willTheProposedInsuredBeOwner, whoWillBePayor, applicationSignedBy, indLegalGuardianPolicyOwner, 
						firstName_LG, lastName_LG, SSN_LG, DoesInsuredHaveDL, DLNo, IssueState, ExpirationDate, 
						earned, unEarned, netWorth, isProposedInsuredUSCitizen, countryofCitizenship, 
						doesProposedInsuredHoldGreenCard, greenCardNumber, GCExpirationDate, doesProposedInsuredHoldUSVisa, 
						typeOfVisa, visaExpirationDate, visaNumber, provideDetails);
				
				ownerInfoJuvenilePageAction = ownerInformationPageAction.enterOwnerInformationData(state, willTheProposedInsuredBeOwner, typeOwner, relnProposedIns,
						ownerFirstName, ownerLastName, ownerSSN, strDOB, ownerBirthState, ownerGender, 
						ownerMaritalStatus, ownerDL_State, strDLExpDate, isMultipleOwner);
				
				ownerInfoContPageAction = ownerInfoJuvenilePageAction.enterOwnerInformationData(applicationSignedBy, 
						indLegalGuardianPolicyOwner, state, 
						willTheProposedInsuredBeOwner, typeOwner, relnProposedIns, ownerFirstName, ownerLastName, ownerSSN,
						strDOB, ownerBirthState, ownerGender, ownerMaritalStatus, ownerDL_State, strDLExpDate, 
						isMultipleOwner);
				
				ownerInfoContJuvenilePageAction = ownerInfoContPageAction.enterOwnerinfoCont(willTheProposedInsuredBeOwner, 
						typeOwner);
				
				multipleOwnersPageAction = ownerInfoContJuvenilePageAction.enterOwnerinfoCont(typeOwner);
				
				premiumPayorInfoPageAction = multipleOwnersPageAction.enterMultipleOwners(willTheProposedInsuredBeOwner, isMultipleOwner, mulOwnerFirst, mulOwnerLast, 
						mOwner_Street, mOwner_city, mOwner_State, mOwner_ZIPCode, mOwner_DOB, mOwner_SSN);
				
				beneficiaryInfoPageAction = premiumPayorInfoPageAction.enterPremiumPayorInfo(whoWillBePayor, payorRelationship, 
						payorEntityName, payorFirstName, payorLastName, payorStreet, payorCity, payorState, payorZIP, 
						payorCountry, payorYrsAtAdd, payorSSN, payorDOB, payorBirthState);
				
				termConversionPageAction = beneficiaryInfoPageAction.detailsOfPrimaryBenef(numOfPB, strShareIndicator, strDeceasedSharePaid, strRelationshipBene1, 
						strFirstNameBene1, strLastNameBene1, strGenderBene1, strSSNBene1, strSharePerBene1, strEntityNameBene1, strCorporateOfficer_Bene1, 
						strTitle_Bene1, strStateIncorporation_Bene1, strOtherRelation_Bene1, strRelationshipBene2, strFirstNameBene2, strLastNameBene2, strGenderBene2, 
						strSSNBene2, strSharePerBene2, strEntityNameBene2, strOtherRelation_Bene2, strRelationshipBene3, strFirstNameBene3, strLastNameBene3, strGenderBene3, 
						strSSNBene3, strSharePerBene3, strEntityNameBene3, strOtherRelation_Bene3, strRelationshipBene4, strFirstNameBene4, strLastNameBene4, strGenderBene4, 
						strSSNBene4, strSharePerBene4, strEntityNameBene4, strOtherRelation_Bene4, strRelationshipBene5, strFirstNameBene5, strLastNameBene5, strGenderBene5, 
						strSSNBene5, strSharePerBene5, strEntityNameBene5, strOtherRelation_Bene5);
					
				wholeLifeCoverageRidersAction = termConversionPageAction.enterDataTermConversion(isTermConversion);	
				
				acceleratedQualifiersPageAction = wholeLifeCoverageRidersAction.enterDataWholeLifeCoverageRiders(product, illustration,
						limitedPayPeriod, baseFaceAmount, initialBasePremium, cashWithApp, premiumAmount, dividendOption,otherDividendOpt, 
						indexedDividendAllocation, nonforfeiture, is_WPD,isAcceleratorPaidRider, isBlendAcceleratorRider, billedpremium,premium1035,premium_non1035,is_EBIR,targetFaceAmt, annualpremium,
						isSameInsuredTermRider,sitr_amount,sitr_GuaranteedPeriod, waiver_conversion_option,is_cbr, cbr_unit, is_cpuad, cpuad_amount, isSurvivorPurchaseOptionRider,SPO_amount,SPO_first,SPO_last, isTermConversion);
				
				
				survivorPurchaseOptionPageAction = acceleratedQualifiersPageAction.enterAUWData(isTermConversion, isAUW);
				
				childrenBenefitRiderAction = survivorPurchaseOptionPageAction.enterSurvivorPurchaseOption(isSurvivorPurchaseOptionRider);
				
				personalInformationAction = childrenBenefitRiderAction.detailsOfChildrenBenefitRider(child_firstname,child_lastname,child_relationship,child_gender,child_height_ft,child_height_in,child_weight,child_dob,child_all_listed);	
				
				perInfoSurviPurchaseOptPageAction = personalInformationAction.enterDataPersonalInformation(tobacco_Uses, isCompletingHealthInformation);
				
				perInfocoveredpersonPageAction = perInfoSurviPurchaseOptPageAction.enterPerInfoSurvivorPurchaseOpt(isSurvivorPurchaseOptionRider);
				healthQuestionsPageAction = perInfocoveredpersonPageAction.enterPerInfocoveredperson();
				

				healthQueContPageAction = healthQuestionsPageAction.enterHealthQuestions(isTermConversion, isCompletingHealthInformation);
				healthQuestionsSPOPageAction = healthQueContPageAction.enterHealthQueCont(isTermConversion, isCompletingHealthInformation);
				
				healthQuestionscoveredpersonPageAction = healthQuestionsSPOPageAction.detailsOfSPOHealthQuestion();

				healthQuestionsChildrenPageAction = healthQuestionscoveredpersonPageAction.detailsOfCPHealthQuestion();
				
				familyHistoryPageAction = healthQuestionsChildrenPageAction.detailsOfChildrenHealthQuestion();
				familyHistorySPOPageAction = familyHistoryPageAction.enterFamilyHistory(isTermConversion, isCompletingHealthInformation);
				
				familyHistoryCPPageAction = familyHistorySPOPageAction.enterFamilyHistorySPO(isTermConversion, isCompletingHealthInformation);
				HIVconsentAction = familyHistoryCPPageAction.enterFamilyHistoryCP(isTermConversion, isCompletingHealthInformation);
				
				existingInsuranceAction = HIVconsentAction.enterDataHIVConsent(state, isSurvivorPurchaseOptionRider);
				premiumInformationAction = existingInsuranceAction.enterDataExistingInsurance(isExistingInsurance, 
						isPolicyReplacing, is1035Exchange);
				usaPatriotActAction = premiumInformationAction.enterDataPremiumInformation(paymentMode, otherMode);
				temporaryInsuranceAgreementAction = usaPatriotActAction.enterDataUSAPatriotAct(willTheProposedInsuredBeOwner, 
						isMultipleOwner);
				thirdPartyOptionPageAction = temporaryInsuranceAgreementAction.enterDataTempInsAgreement(cashWithApp);
				
				validateAndLockDataAction = thirdPartyOptionPageAction.enterThirdPartyOption(state);
				                                     
				signatureMethodAction = validateAndLockDataAction.enterDataValAndLockData();
				eSigdiclosuresAction = signatureMethodAction.enterDataSignatureMethod();
				eSignatureconsentAction = eSigdiclosuresAction.enterDataESigDisclosures(willTheProposedInsuredBeOwner, 
						isMultipleOwner, isSurvivorPurchaseOptionRider, whoWillBePayor, indLegalGuardianPolicyOwner);
				eSignaturepartiesAction = eSignatureconsentAction.enterDataESignatureConsent(willTheProposedInsuredBeOwner, 
						isMultipleOwner, isSurvivorPurchaseOptionRider, whoWillBePayor, indLegalGuardianPolicyOwner);
				representativeInformationAction = eSignaturepartiesAction.enterDataeSignatureParties(willTheProposedInsuredBeOwner, 
						isMultipleOwner, isSurvivorPurchaseOptionRider, whoWillBePayor, indLegalGuardianPolicyOwner);
				applyESignatureSubmitAction = representativeInformationAction.enterDataRepresentativeInfo(isAdditionalRepresentatives, 
						AddRepresentativesCount, representativesName1,
						sharePercentage1, representativesCode1, representativesName2, sharePercentage2, 
						representativesCode2, representativesName3, sharePercentage3, representativesCode3, representativesName4, sharePercentage4, representativesCode4);
					
				applyESignatureSubmitAction.enterApplyESignatureSubmit();
			} catch (Exception e) {
				e.printStackTrace();
				// genericFunction.takeScreenshot("Error");
			} finally {

			}
		}
	}

