package com.qa.pageActions;

import com.qa.pages.FamilyHistoryPage;
import com.relevantcodes.extentreports.LogStatus;
import static com.qa.pageActions.AcceleratedQualifiersPageAction.flagAUW;
import static com.qa.pageActions.AcceleratedQualifiersPageAction.flagAUW2_Qualified;
import static com.qa.pageActions.AcceleratedQualifiersPageAction.flagIdo_AUW;

public class FamilyHistoryPageAction extends FamilyHistoryPage{
	public FamilyHistoryPageAction() {
		super();
	}
	
	//Actions
	public FamilyHistorySPOPageAction enterFamilyHistory(String isTermConversion, String isCompletingHealthInformation) {
		boolean flagHealthQuestion = (isTermConversion.equalsIgnoreCase("Yes") || flagAUW) && (!flagIdo_AUW);
		if(flagAUW2_Qualified) {
			return new FamilyHistorySPOPageAction();
		}
		if(isCompletingHealthInformation.equalsIgnoreCase("No") && flagAUW) {
			return new FamilyHistorySPOPageAction();
		}
		if(flagHealthQuestion) {
			try {
				extentTest.log(LogStatus.INFO, " - Family History Page - ");
				switchToFrame(frameFamilyHistory);	
				ClickElement(rdoDoSibling_No, "Do you have Siblings_no");
				EnterText(txtAgeMother, "65", "Age Mother");
				EnterText(txtAgeFather, "68", "Age father");
				ClickElement(rdoCancerMother, "CancerMother");
				ClickElement(rdoCancerFather, "CancerFather");
				ClickElement(rdoHeartMother, "HeartMother");
				ClickElement(rdoHeartFather, "HeartFather");
				ClickElement(rdoDiabetesMother, "DiabetesMother");
				ClickElement(rdoDiabetesFather, "DiabetesFather");
				takeScreenshot("FamilyHistoryPage");
				ClickElement(btnNext, "Next button");
				switchToDefault();
				Thread.sleep(5000);
				return new FamilyHistorySPOPageAction();
			}
			catch(Exception e) {
				e.printStackTrace();
				return new FamilyHistorySPOPageAction();
			}
		}
		else {
			return new FamilyHistorySPOPageAction();
		}
	}	
}
