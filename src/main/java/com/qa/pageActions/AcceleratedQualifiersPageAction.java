package com.qa.pageActions;

import com.qa.pages.AcceleratedQualifiersPage;
import com.relevantcodes.extentreports.LogStatus;

import static com.qa.pageActions.ProposedInsuredInfoAction.insuredAge;
import static com.qa.pageActions.WholeLifeCoverageRidersAction.faceAmount;

public class AcceleratedQualifiersPageAction extends AcceleratedQualifiersPage{
	public AcceleratedQualifiersPageAction() {
		super();
	}
	public static boolean flagAUW = false;
	public static boolean flagIdo_AUW = false;
	public static boolean flagAUW2_Qualified = false;
	//Actions
	public SurvivorPurchaseOptionPageAction enterAUWData(String isTermConversion, String isAUW) throws InterruptedException {
		flagAUW = insuredAge>=18 && insuredAge<=50 && faceAmount <= 1000000 && isTermConversion.equalsIgnoreCase("No");
		if(flagAUW) {
			extentTest.log(LogStatus.INFO, " - Accelerated Qualifiers Page - ");
			switchToFrame(frameAcceleratedQualifiers);
			ClickElement(rdo_No_Q1, "rdo_No_Q1");
			ClickElement(rdo_No_Q2, "rdo_No_Q2");
			ClickElement(rdo_No_Q3, "rdo_No_Q3");
			ClickElement(cb_Authorization, "cb_Authorization");
			ClickElement(btnInitiateProcess, "InitiateProcess");
//			if(SelectionIndAsYes(isAUW, "AUW")) {
//				ClickElement(rdo_Ido, "rdo_Ido");
//				EnterText(txtPhoneNumber, "9632587412", "Phone number");
//				flagIdo_AUW = true;
//			}else {
//				ClickElement(rdo_Idonot, "rdo_Idonot");
//			}
			
//			Thread.sleep(15000);
			scrollIntoView(rdo_No_Q3, driver);
			takeScreenshot("AcceleratedQualifierPage");
			Thread.sleep(30000);
			try {
				if(labelAUWQualified.isDisplayed()) {
					flagAUW2_Qualified = true;				
				}
			}catch(Exception e) {
				
			}
//			Thread.sleep(90000);
			takeScreenshot("AcceleratedQualifierPage");
			ClickElement(btnNext, "Next button");
			switchToDefault();
			Thread.sleep(5000);
			return new SurvivorPurchaseOptionPageAction();	
		}else {
			return new SurvivorPurchaseOptionPageAction();
		}
	}
}
