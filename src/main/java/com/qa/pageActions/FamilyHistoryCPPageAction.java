package com.qa.pageActions;

import com.qa.pages.FamilyHistoryCPPage;
import com.qa.pages.FamilyHistoryPage;
import com.qa.pages.FamilyHistorySPOPage;
import com.relevantcodes.extentreports.LogStatus;
import static com.qa.pageActions.AcceleratedQualifiersPageAction.flagAUW;
import static com.qa.pageActions.AcceleratedQualifiersPageAction.flagAUW2_Qualified;
import static com.qa.pageActions.AcceleratedQualifiersPageAction.flagIdo_AUW;
import static com.qa.pageActions.Illustration_RidersPageAction.waiverpremiumdisabilityflag;

public class FamilyHistoryCPPageAction extends FamilyHistoryCPPage{
	public FamilyHistoryCPPageAction() {
		super();
	}
	
	//Actions
	public HIVConsentAction enterFamilyHistoryCP(String isTermConversion, String isCompletingHealthInformation) throws InterruptedException {
		if(waiverpremiumdisabilityflag == true ) {	
				extentTest.log(LogStatus.INFO, " - Covered person Family History Page - ");
				switchToFrame(frameFamilyHistoryCP);	
				ClickElement(rdoDoSibling_No, "Do you have Siblings_no");
				EnterText(txtAgeMother, "65", "Age Mother");
				EnterText(txtAgeFather, "68", "Age father");
				ClickElement(rdoCancerMother, "CancerMother");
				ClickElement(rdoCancerFather, "CancerFather");
				ClickElement(rdoHeartMother, "HeartMother");
				ClickElement(rdoHeartFather, "HeartFather");
				ClickElement(rdoDiabetesMother, "DiabetesMother");
				ClickElement(rdoDiabetesFather, "DiabetesFather");
				takeScreenshot("FamilyHistoryPageCP");
				ClickElement(btnNext, "Next button");
				switchToDefault();
				Thread.sleep(3000);
				return new HIVConsentAction();	
			
		}
		else {
			return new HIVConsentAction();
		}
	}	
}
