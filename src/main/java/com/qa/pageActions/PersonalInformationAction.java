package com.qa.pageActions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import com.qa.pages.PersonalInformationPage;
import com.relevantcodes.extentreports.LogStatus;

import static com.qa.pageActions.AcceleratedQualifiersPageAction.flagIdo_AUW;
import static com.qa.pageActions.AcceleratedQualifiersPageAction.flagAUW2_Qualified;

public class PersonalInformationAction extends PersonalInformationPage {
	
	public PersonalInformationAction() {
		super();
	}
	
	//Actions
	public PerInfoSurviPurchaseOptPageAction enterDataPersonalInformation(String tobacco_Uses, String isCompletingHealthInformation) throws InterruptedException, AWTException {
		if(flagAUW2_Qualified) {
			return new PerInfoSurviPurchaseOptPageAction();
		}
		if(flagIdo_AUW) {
			return new PerInfoSurviPurchaseOptPageAction();
		}
		else {
			extentTest.log(LogStatus.INFO, " - Personal Information Page - ");
			switchToFrame(framePersonalInformation);
			ClickElement(rdoQuestionNo_1, "Question_1_No");
			ClickElement(rdoQuestionNo_2, "Question_2_No");
			ClickElement(rdoQuestionNo_3, "Question_3_No");
			ClickElement(rdoQuestionNo_4, "Question_4_No");
			ClickElement(rdoQuestionNo_5, "Question_5_No");
			ClickElement(rdoQuestionNo_6, "Question_6_No");
			ClickElement(rdoQuestionNo_7, "Question_7_No");
			ClickElement(rdoQuestionNo_8, "Question_8_No");
			ClickElement(rdoQuestionNo_9, "Question_9_No");
			ClickElement(rdoQuestionNo_10, "Question_10_No");
			
			if(SelectionIndAsYes(tobacco_Uses, "Tobacco_Uses")) {
				ClickElement(rdoTobacco_Yes, "Tobacco_Yes");
				ClickElement(btnTobaccoDetails, "TobaccoDetails");
				switchToDefault();
				Thread.sleep(5000);
				switchToFrame(frameTobaccoDetails);
				ClickElement(rdoFormerUse, "FormerUse");
				EnterText(txtTypeOfNicotine, "Smoking", "Nicotine");
				EnterText(txtQuitNicotine, "05/2011", "QuitNicotine");
				Robot r = new Robot();
				r.keyPress(KeyEvent.VK_TAB);
				r.keyRelease(KeyEvent.VK_TAB);
				r.keyPress(KeyEvent.VK_ENTER);
				r.keyRelease(KeyEvent.VK_ENTER);
				switchToDefault();
				Thread.sleep(5000);
				switchToFrame(framePersonalInformation);
			}
			else {
				ClickElement(rdoTobacco_No, "Tobacco_No");
			}
			
			
			ClickElement(rdoQuestionNo_12, "Question_12_No");
			ClickElement(rdoQuestionNo_13, "Question_13_No");
			scrollIntoView(rdoCompletingHealthInformationNo, driver);
			if(isEnabledElement(rdoCompletingHealthInformationNo)) {
				selectYesNoRdoBtn(isCompletingHealthInformation, "Completing Health Information", rdoCompletingHealthInformationYes, rdoCompletingHealthInformationNo);
			}
			takeScreenshot("PersonalInformationPage");
			ClickElement(btnNext, "Next button");
			switchToDefault();
			Thread.sleep(5000);
			return new PerInfoSurviPurchaseOptPageAction();
		}
	}
}
