package com.qa.pageActions;

import com.qa.pages.FamilyHistoryPage;
import com.qa.pages.FamilyHistorySPOPage;
import com.relevantcodes.extentreports.LogStatus;
import static com.qa.pageActions.AcceleratedQualifiersPageAction.flagAUW;
import static com.qa.pageActions.AcceleratedQualifiersPageAction.flagAUW2_Qualified;
import static com.qa.pageActions.AcceleratedQualifiersPageAction.flagIdo_AUW;
import static com.qa.pageActions.Illustration_RidersPageAction.survivorPurchaseOptionRiderflag_illus;

public class FamilyHistorySPOPageAction extends FamilyHistorySPOPage{
	public FamilyHistorySPOPageAction() {
		super();
	}
	
	//Actions
	public FamilyHistoryCPPageAction enterFamilyHistorySPO(String isTermConversion, String isCompletingHealthInformation) throws InterruptedException {
		if(survivorPurchaseOptionRiderflag_illus == true ) {	
				extentTest.log(LogStatus.INFO, " - survivor Purchase Option Rider Family History Page - ");
				switchToFrame(frameFamilyHistorySPO);	
				ClickElement(rdoDoSibling_No, "Do you have Siblings_no");
				EnterText(txtAgeMother, "65", "Age Mother");
				EnterText(txtAgeFather, "68", "Age father");
				ClickElement(rdoCancerMother, "CancerMother");
				ClickElement(rdoCancerFather, "CancerFather");
				ClickElement(rdoHeartMother, "HeartMother");
				ClickElement(rdoHeartFather, "HeartFather");
				ClickElement(rdoDiabetesMother, "DiabetesMother");
				ClickElement(rdoDiabetesFather, "DiabetesFather");
				takeScreenshot("FamilyHistoryPageSPO");
				ClickElement(btnNext, "Next button");
				switchToDefault();
				Thread.sleep(3000);
				return new FamilyHistoryCPPageAction();	
			
		}
		else {
			return new FamilyHistoryCPPageAction();
		}
	}	
}
