package com.qa.pageActions;

import com.qa.pages.WholeLifeCoverageRidersPage;
import com.relevantcodes.extentreports.LogStatus;
import static com.qa.pageActions.ProposedInsuredInfoAction.insuredAge;

public class WholeLifeCoverageRidersAction extends WholeLifeCoverageRidersPage{

	public WholeLifeCoverageRidersAction() {
		super();
	}
	public static int faceAmount;
	public static boolean childbenefitRiderflag;
	public static boolean survivorPurchaseOptionRiderflag;
	//Actions
	public AcceleratedQualifiersPageAction enterDataWholeLifeCoverageRiders(String product,String illustration,String limitedPayPeriod, 
			String baseFaceAmount, String initialBasePremium, String cashWithApp, String premiumAmount, String dividendOption,
			String otherDividendOpt, String indexedDividendAllocation, String nonforfeiture, String is_WPD,String isAcceleratorPaidRider, 
			String isBlendAcceleratorRider, String billedpremium,String premium1035,String premium_non1035, String is_EBIR,
			String targetFaceAmt,String annualpremium,String isSameInsuredTermRider,String sitr_amount,String sitr_GuaranteedPeriod,String waiver_conversion_option,
			String is_cbr,String cbr_unit, String is_cpuad,String cpuad_amount,String isSurvivorPurchaseOptionRider, String SPO_amount,String SPO_first,String SPO_last,String isTermConversion) throws InterruptedException {
		childbenefitRiderflag = false;
		survivorPurchaseOptionRiderflag = false;

		extentTest.log(LogStatus.INFO, " - Whole Life Coverage Riders Page - ");
		switchToFrame(frameWholeLifeCoverageRiders);
		if(illustration.equalsIgnoreCase("No")) {
			if(product.equalsIgnoreCase("Whole Life Select")) {		
				ComboSelectValue(dropdownLimitedPayPeriod, limitedPayPeriod, "LimitedPayPeriod");
			}
			
			EnterText(txtBaseFaceAmount, baseFaceAmount, "Base Face Amount");
			EnterText(txtInitialBasePremium, initialBasePremium, "Initial Base Premium");
		}
		if(SelectionIndAsYes(cashWithApp, "CashWithApp")) {
			ClickJSElement(rdoCashWithApp_Yes, "CashWithApp_Yes");
			EnterText(txtPremiumAmount, premiumAmount, "PremiumAmount");
		}else {
			ClickJSElement(rdoCashWithApp_No, "CashWithApp_No");
		}
		if(illustration.equalsIgnoreCase("No")) {	
			ComboSelectValue(dropdownDividendOptions, dividendOption, "DividendOption");
			
			if(dividendOption.equalsIgnoreCase("Other")) {
				EnterText(txtOtherDividendOption, otherDividendOpt, "Other Dividend Option");
			}else if(dividendOption.equalsIgnoreCase("Accumulate at Interest (Opt 2)") || dividendOption.equalsIgnoreCase("Paid-Up Additions (Opt 4)")) {
				EnterText(txtIndexedDividendAllocation, indexedDividendAllocation, "Indexed Dividend Allocation");
			}
			
			if(SelectionIndAsYes(is_WPD, "WaiverofPremiumfordisability")) {
				ClickElement(waiverpremiumfordisability, "waiver premium for disability");
				ComboSelectValue(coveredperson_dropdown, "Proposed Insured", "covered person");
			}
			if(SelectionIndAsYes(isAcceleratorPaidRider, "AcceleratorPaidAdditionsRider")) {
				ClickElement(cbAcceleratorRider, "cbAcceleratorRider");
				EnterText(txtBilledPrem, billedpremium, "BilledPrem");
				if(isTermConversion.equalsIgnoreCase("No"))
					EnterText(txt1035Premium, premium1035, "1035Premium");
				EnterText(txtNon1035Premium, premium_non1035, "txtNon1035Premium");
			}
	
			if(SelectionIndAsYes(isBlendAcceleratorRider, "BlendAcceleratorPaidUpAdditionRider")) {
				ClickElement(cbBlendRider, "cbBlendRider");
				EnterText(txtBilledPremBlendAccelerator, billedpremium, "BilledPremBlendAccelerator");
				if(isTermConversion.equalsIgnoreCase("No"))
					EnterText(txt1035PremiumBlendAccelerator, premium1035, "1035PremiumBlendAccelerator");
				EnterText(txtNon1035PremiumBlendAccelerator, premium_non1035, "Non1035PremiumBlendAccelerator");
				
				
				EnterText(txtTargetFaceAmt, targetFaceAmt, "txtTargetFaceAmt");
				EnterText(txtAnnualFaceAmt, annualpremium, "txtAnnualFaceAmt");
			}
			if((isBlendAcceleratorRider.equalsIgnoreCase("No"))&&(isAcceleratorPaidRider.equalsIgnoreCase("Yes"))&&(is_EBIR.equalsIgnoreCase("Yes"))) {
				ClickElement(cbEBIR, "cbEBIR");
				EnterText(txtTargetFaceAmt, targetFaceAmt, "txtTargetFaceAmt");
				EnterText(txtAnnualFaceAmt, annualpremium, "txtAnnualFaceAmt");
			}
			if(SelectionIndAsYes(isSameInsuredTermRider, "SameInsuredTermRider")) {
				ClickElement(cbSameInsuredTermRider, "cbBlendRider");
				EnterText(txtAmount_SITR, sitr_amount, "AnnualFaceAmt");
				ComboSelectValue(dropdownGuaranteedPeriod_SITR, sitr_GuaranteedPeriod, "GuaranteedPeriod_SITR");
				if(waiver_conversion_option.equalsIgnoreCase("Yes"))
					ClickElement(waiver_conversion_option_yes, "waiver conversion option as yes");
				else
					ClickElement(waiver_conversion_option_no, "waiver conversion option as no");
					
			}
			if((isBlendAcceleratorRider.equalsIgnoreCase("Yes"))||(isAcceleratorPaidRider.equalsIgnoreCase("Yes"))){
				if(is_cpuad.equalsIgnoreCase("Yes")) {
					ClickElement(cb_cpuad, "checkbox CPUAD");
					EnterText(cpuad_monthlybenefitamount, cpuad_amount, "cpuad monthly benefit amount");
				}
			}
			if(is_cbr.equalsIgnoreCase("Yes")) {
				ClickElement(cbRider, "children's benefit Rider");
				ComboSelectValue(cbRider_unit, cbr_unit, "chilren's benefit Rider unit");
				childbenefitRiderflag = true;
			}
			if(SelectionIndAsYes(isSurvivorPurchaseOptionRider, "SurvivorPurchaseOptionRider")) {
				ClickElement(cbSurvivorPurchaseOptionRider, "cbBlendRider");
				EnterText(txtFaceAmt_SPO, SPO_amount, "FaceAmt_SPO");
				EnterText(txtFirst_SPO, SPO_first, "First_SPO");
				EnterText(txtLast_SPO, SPO_last, "txtLast_SPO");
				survivorPurchaseOptionRiderflag = true;
			}
		}
		ComboSelectValue(drodownNonforfeiture, nonforfeiture, "Nonforfeiture Dropdown");
		
		faceAmount=Integer.parseInt(baseFaceAmount);
		if(insuredAge>=18 && insuredAge<=50 && faceAmount <= 1000000 && isTermConversion.equalsIgnoreCase("No")) {
			ClickJSElement(rdoConcurrentCoverage_No, "ConcurrentCoverage_No");
		}
		
		takeScreenshot("WholeLifeCoverageRidersPage");
		ClickElement(btnNext, "Next button");
		switchToDefault();
		Thread.sleep(5000);
		return new AcceleratedQualifiersPageAction();
	}

}

