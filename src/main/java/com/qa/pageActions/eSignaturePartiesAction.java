package com.qa.pageActions;

import com.qa.pages.eSignaturePartiesPage;
import com.qa.util.Wait;
import com.relevantcodes.extentreports.LogStatus;

import static com.qa.pageActions.Illustration_RidersPageAction.survivorPurchaseOptionRiderflag_illus;
import static com.qa.pageActions.ProposedInsuredInfoAction.flagJuvenile;
import static com.qa.pageActions.WholeLifeCoverageRidersAction.survivorPurchaseOptionRiderflag;

public class eSignaturePartiesAction extends eSignaturePartiesPage{
	
	public eSignaturePartiesAction() {
		super();		
	}
	
	Wait wait = new Wait();
	
	//Actions 
	public RepresentativeInformationAction enterDataeSignatureParties(String willTheProposedInsuredBeOwner, 
			String isMultipleOwner, String isSurvivorPurchaseOptionRider, String whoWillBePayor,
			String indLegalGuardianPolicyOwner) throws InterruptedException {
		
		extentTest.log(LogStatus.INFO, " - eSignatureParties Page - ");
		switchToFrame(frameESignatureParties);
		EnterText(txtCity, "Alska", "City");
		if(!flagJuvenile) {
			ClickElement(cbInsuredAgreement, "InsuredAgreement Checkbox");	
		}else {
			if(indLegalGuardianPolicyOwner.equalsIgnoreCase("No")) {
				ClickElement(cb_LG, "Legal Gurdian Checkbox");
			}
		}
		if(willTheProposedInsuredBeOwner.equalsIgnoreCase("No")) {
			ClickElement(cbOwner, "Owner Checkbox");
			if(isMultipleOwner.equalsIgnoreCase("Yes")) {
				ClickElement(cbMO, "Owner Checkbox");
			}
		}
		if(survivorPurchaseOptionRiderflag == true || survivorPurchaseOptionRiderflag_illus == true) {
			ClickElement(cbSurvivor, "Owner Checkbox");
		}
		if(whoWillBePayor.equalsIgnoreCase("Other")) {
			ClickElement(cbPayor, "Payor Checkbox");
		}
		
		
		ClickElement(btnApply, "Apply Button");
		Thread.sleep(5000);
		wait.waitForPageLoad(driver);
		takeScreenshot("eSignatureParties");
		ClickElement(btnYesContinue, "YesContinue Button");
		switchToDefault();
		Thread.sleep(5000);
		return new RepresentativeInformationAction();
	}
}
