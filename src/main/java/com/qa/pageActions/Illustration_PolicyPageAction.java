package com.qa.pageActions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import com.qa.pages.Illustration_PolicyPage;
import com.relevantcodes.extentreports.LogStatus;

public class Illustration_PolicyPageAction extends Illustration_PolicyPage{
	
	public Illustration_PolicyPageAction() {
		super();
	}
	
	//Action
	public Illustration_RidersPageAction enterDataPolicyPage(String gender, String useUnisexRate, String primaryInsuredClass,
			String permanentFlatExtra, String temporaryFlatExtra, String forYears, String inputMethods,
			String intialBaseFaceAmount, String premiumAmount, String dividends, String dividendOptionChange,
			String percentageDividendScale, String scheduleLoansWithdrawals, String withdrawalOptions,
			String loanInterest, String paymentMode, String isTermConversion) throws AWTException, InterruptedException {
		extentTest.log(LogStatus.INFO, " - Illustration Policy Page - ");
		switchToFrame(frameIllustrationPolicy);
		ComboSelectValue(dropdownGender, gender, "Gender");
		if(SelectionIndAsYes(useUnisexRate, "UseUnisexRates")){
			ClickJSElement(cbUseUnisexRates, "UseUnisexRates");
		}
		ComboSelectValue(dropdownPrimaryInsuredClass, primaryInsuredClass, "Primary Insured Class");
		ClickJSElement(btnRating, "Rating");
		Thread.sleep(3000);
		switchToDefault();
		switchToFrame(frameRatingDetails);
		EnterText(txtPermanentFlatExtra, permanentFlatExtra, "PermanentFlatExtra");
		EnterText(txtTemporaryFlatExtra, temporaryFlatExtra, "TemporaryFlatExtra");
		EnterText(txtForYears, forYears, "ForYears");
		ClickJSElement(btnSave, "btnSave");
		Thread.sleep(3000);
		switchToDefault();
		switchToFrame(frameIllustrationPolicy);
		
		ComboSelectValue(dropdownInputMethods, inputMethods, "InputMethods");
		if(inputMethods.equalsIgnoreCase("Base Face Amount")) {
			EnterText(txtIntialBaseFaceAmount, intialBaseFaceAmount, "IntialBaseFaceAmount");
			ClickElement(txtInputMethods, "InputMethods");
		}else {
			EnterText(txtPremiumAmount, premiumAmount, "PremiumAmount");
			ClickElement(txtInputMethods, "InputMethods");
		}
		
		ComboSelectValue(dropdownDividends, dividends, "dropdownDividends");
		if(SelectionIndAsYes(dividendOptionChange, "DividendOptionChange")) {
			ClickJSElement(cbDividendOptionChange, "DividendOptionChange");
			Thread.sleep(2000);
			switchToDefault();
			switchToFrame(frameDividendOptionChange);
			ClickElement(txtFromAgeDividendOptionChange, "Age");
			ClickJSElement(btnSaveDividendOption, "btnSaveDividendOptionChange");
			Thread.sleep(2000);
			switchToDefault();
			switchToFrame(frameIllustrationPolicy);
			EnterText(txtPercentageDividendScale, percentageDividendScale, "PercentageDividendScale");
		}else {
			if(dividends.equalsIgnoreCase("Option 4 � Paid-Up Additions") || dividends.equalsIgnoreCase("Option 2 � Accumulate at Interest")){
				EnterText(txtPercentageDividendScale, percentageDividendScale, "PercentageDividendScale");
			}
		}
		if(SelectionIndAsYes(scheduleLoansWithdrawals, "ScheduleLoansWithdrawals")) {
			ClickJSElement(cbScheduleLoansWithdrawals, "ScheduleLoansWithdrawals");
			ComboSelectValue(dropdownWithdrawalOptions, withdrawalOptions, "WithdrawalOptions");
			ComboSelectValue(dropdownLoanInterest, loanInterest, "LoanInterest");
		}
		
		ComboSelectValue(dropdownPaymentMode, paymentMode, "PaymentMode");
		if(SelectionIndAsYes(isTermConversion, "TermConversion")) {
			ClickJSElement(rdoIsTermConversion_Yes, "IsTermConversion_Yes");
		}else {
			ClickJSElement(rdoIsTermConversion_No, "IsTermConversion_No");
		}
		
		takeScreenshot("Illustration_PolicyPage");
		ClickElement(btnNext, "Next Button");
		switchToDefault();
		Thread.sleep(5000);
		return new Illustration_RidersPageAction();
		
		
	}

}
