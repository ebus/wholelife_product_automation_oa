package com.qa.pageActions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import com.qa.pages.Illustration_RidersPage;
import com.relevantcodes.extentreports.LogStatus;

public class Illustration_RidersPageAction extends Illustration_RidersPage{
	public Illustration_RidersPageAction() {
		super();
	}
	public static boolean survivorPurchaseOptionRiderflag_illus;
	public static boolean childbenefitRiderflag_illus;
	public static boolean waiverpremiumdisabilityflag;
	//Action
	public ProposedInsuredInfoAction enterDataRiderPage(String waiverPremiumDisability, String coveredPerson, String firstName_CP, String lastName_CP,
			String dob_CP, String gender_CP, String classCP, String durationCP,String childBenefitRider, String CBR_unit, String CBR_Duration,
			String isPUARider, String str1035SinglePremium, String monthReceived,
			String strNon1035SinglePremium, String isEnhancedBlendedInsuranceRider, String EBIRSpecifiedFace,
			String EBIRAmount, String targetPersistencyAge, String duration_EnhancedBlendedInsRider, 
			String isSameInsuredTermRider, String plan_SameInsTermRider, String class_SameInsTermRider,
			String isSurvivorPurchaseOption, String firstName_SurvivorPurchaseOption, 
			String lastName_SurvivorPurchaseOption, String gender_SurvivorPurchaseOption,
			String class_SurvivorPurchaseOption, String coverageAmt_SurvivorPurchaseOption,
			String duration_SurvivorPurchaseOption) throws InterruptedException, AWTException {
		
		survivorPurchaseOptionRiderflag_illus = false;
		childbenefitRiderflag_illus = false;
		waiverpremiumdisabilityflag = false;
		extentTest.log(LogStatus.INFO, " - Illustration Riders Page - ");
		switchToFrame(frameIllustrationRiders);
		if(SelectionIndAsYes(waiverPremiumDisability, "WaiverPremiumDisability")) {
			ClickJSElement(cbWaiverPremiumDisability, "WaiverPremiumDisability");
			switchToDefault();
			switchToFrame(frameWaiverPremiumDisability);
			Thread.sleep(2000);
			ComboSelectValue(dropdownCoveredPerson, coveredPerson, "CoveredPerson");
			EnterText(txtFirstName_CP, firstName_CP, "FirstName");
			EnterText(txtLastName_CP, lastName_CP, "txtLastName");
			ComboSelectValue(dropdownGender_CP, gender_CP, "Gender");
			enterDOB(dob_CP);
			ComboSelectValue(dropdownClass, classCP, "Class");
			EnterText(txtDuration, durationCP, "txtDuration");
			ClickJSElement(btnWaiverOK, "WaiverOK");
			switchToDefault();
			switchToFrame(frameIllustrationRiders);
			Thread.sleep(2000);
			waiverpremiumdisabilityflag = true;
		}
		if(SelectionIndAsYes(childBenefitRider, "childBenefitRider")) {
			ClickJSElement(childrenbenefitrider, "childrenbenefitrider");
			EnterText(cbrunits, CBR_unit, "units");
			EnterText(cbrduration, CBR_Duration, "duration");
			childbenefitRiderflag_illus = true;
		}
		if(SelectionIndAsYes(isPUARider, "PUARider")) {
			ClickElement(cbPUARider, "PUARider");
			EnterText(txt1035SinglePremium, str1035SinglePremium, "1035SinglePremium");
			ComboSelectValue(dropdownMonthReceived, monthReceived, "dropdownMonthReceived");
			EnterText(txtNon1035SinglePremium, strNon1035SinglePremium, "txtNon1035SinglePremium");
			ClickElement(btnModalPremiums, "ModalPremiums");
			switchToDefault();
			switchToFrame(framePUARiderPremiums);
			Thread.sleep(2000);
			ClickJSElement(btnOKPUARiderPremiums, "btnOKPUARiderPremiums");
			switchToDefault();
			switchToFrame(frameIllustrationRiders);
			Thread.sleep(2000);
		}
		if(SelectionIndAsYes(isEnhancedBlendedInsuranceRider, "EnhancedBlendedInsuranceRider")) {
			ClickElement(cbEnhancedBlendedInsuranceRider, "EnhancedBlendedInsuranceRider");
			switchToDefault();
			switchToFrame(framePUARiderPremiums);
			Thread.sleep(2000);
			ComboSelectValue(dropdownEBIRSpecifiedFace, EBIRSpecifiedFace, "EBIRSpecifiedFace");
			if(EBIRSpecifiedFace.equalsIgnoreCase("Amount")) {
				EnterText(txtEBIRAmount, EBIRAmount, "EBIRAmount");
			}
			EnterText(txtTargetPersistencyAge, targetPersistencyAge, "TargetPersistencyAge");
			EnterText(txtDuration_EnhancedBlendedInsRider, duration_EnhancedBlendedInsRider, "Duration_EnhancedBlendedInsRider");
			ClickJSElement(btnOK_EnhancedBlendedInsRider, "OK_EnhancedBlendedInsRider");
			switchToDefault();
			switchToFrame(frameIllustrationRiders);
			Thread.sleep(2000);
		}
		if(SelectionIndAsYes(isSameInsuredTermRider, "SameInsuredTermRider")) {
			ClickElement(cbSameInsuredTermRider, "SameInsuredTermRider");
			switchToDefault();
			switchToFrame(framePUARiderPremiums);
			Thread.sleep(2000);
			ComboSelectValue(dropdownPlan_SameInsTermRider, plan_SameInsTermRider, "Plan");
			ComboSelectValue(dropdownClass_SameInsTermRider, class_SameInsTermRider, "Class_SameInsTermRider");
			ClickJSElement(btnOK__SameInsTermRider, "OK__SameInsTermRider");
			switchToDefault();
			switchToFrame(frameIllustrationRiders);
			Thread.sleep(2000);
		}
		if(SelectionIndAsYes(isSurvivorPurchaseOption, "SurvivorPurchaseOption")) {
			ClickElement(cbSurvivorPurchaseOption, "SurvivorPurchaseOption");
			switchToDefault();
			switchToFrame(framePUARiderPremiums);
			Thread.sleep(2000);
			EnterText(txtFirstName_SurvivorPurchaseOption, firstName_SurvivorPurchaseOption, "FirstName");
			EnterText(txtLastName_SurvivorPurchaseOption, lastName_SurvivorPurchaseOption, "lastName");
			ComboSelectValue(dropdownGender_SurvivorPurchaseOption, gender_SurvivorPurchaseOption, "Gender");
			ComboSelectValue(dropdownClass_SurvivorPurchaseOption, class_SurvivorPurchaseOption, "Class");
			EnterText(txtCoverageAmt_SurvivorPurchaseOption, coverageAmt_SurvivorPurchaseOption, "Coverage Amount");
			EnterText(txtDuration_SurvivorPurchaseOption, duration_SurvivorPurchaseOption, "Duration");
			ClickJSElement(btnOK__SurvivorPurchaseOption, "OK__SurvivorPurchaseOption");
			switchToDefault();
			switchToFrame(frameIllustrationRiders);
			Thread.sleep(2000);
			survivorPurchaseOptionRiderflag_illus = true;
		}
		ClickElement(btnNext, "Next");
		switchToDefault();
		Thread.sleep(5000);
		switchToFrame(frameIllustrationRiders);
		ClickElement(btnIllustartion, "Illustartion");
		
		Thread.sleep(30000);
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ALT);
		r.keyPress(KeyEvent.VK_F4);
		r.keyRelease(KeyEvent.VK_F4);
		r.keyRelease(KeyEvent.VK_ALT);
		
		Thread.sleep(2000);
		switchToDefault();
		ClickJSElement(tabApplication, "Application Tab");
		
		Thread.sleep(15000);
		return new ProposedInsuredInfoAction();
	}
	
	public void enterDOB(String dob)
	{	
		String[] dateOfBirth = dob.split("/");
		String strMonth = dateOfBirth[0];
		String strDay = dateOfBirth[1];
		String strYear = dateOfBirth[2];
		txtMonth.sendKeys(strMonth);
		txtYear.sendKeys(strYear);
		txtDay.sendKeys(strDay);
		
		extentTest.log(LogStatus.PASS, dob + " is entered in DOB");	
	}
}
