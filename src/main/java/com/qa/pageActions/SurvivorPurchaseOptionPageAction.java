package com.qa.pageActions;
import com.qa.pageActions.OwnerInfoContPageAction;
import com.qa.pages.SurvivorPurchaseOptionPage;
import com.relevantcodes.extentreports.LogStatus;

import static com.qa.pageActions.Illustration_RidersPageAction.survivorPurchaseOptionRiderflag_illus;
import static com.qa.pageActions.WholeLifeCoverageRidersAction.survivorPurchaseOptionRiderflag;

public class SurvivorPurchaseOptionPageAction extends SurvivorPurchaseOptionPage {
	public SurvivorPurchaseOptionPageAction() {
		super();
	}
	
	//Actions
	public ChildrenBenefitRiderAction enterSurvivorPurchaseOption(String isSurvivorPurchaseOptionRider) {
		if(survivorPurchaseOptionRiderflag == true || survivorPurchaseOptionRiderflag_illus == true) {
			try {
				extentTest.log(LogStatus.INFO, " - Survivor Purchase Option Page - ");
				switchToFrame(frameSurvivorPurchaseOption);
				EnterText(txtSSN, "234365673", "SSN");
				enterDOB("04/04/1961",txtMonth,txtDay,txtYear);
				ClickElement(rdoMale, "Male");
				ComboSelectValue(dropdownBirthState, "Alaska", "BirthState");
				ComboSelectValue(dropdownRelationshipPropIns, "Cousin", "RelationshipPropIns");
				ClickElement(cbSurvivorPurchase, "SurvivorPurchase");
				EnterText(txtYearsAddress, "10", "YearsAddress");
				EnterText(txtPhoneNumber, "3241576453", "PhoneNumber");
				EnterText(email, "abc@abc.com", "email");
				ClickElement(rdoDriverLicense_Yes, "DriverLicense_Yes");
				EnterText(txtDriverLicenseNumber, "67345", "DriverLicenseNumber");
				ComboSelectValue(dropdownIssueState, "Alaska", "IssueState");
				enterDOB("04/04/2030", txtMonthDL, txtDayDL, txtYearDL);
				ClickElement(rdoUSCitizen_Yes, "USCitizen_Yes");
				ComboSelectValue(dropdownEmployementStatus, "Retired", "EmployementStatus");
				takeScreenshot("SurvivorPurchaseOptionPage");
				ClickElement(btnNext, "Next Button");
				switchToDefault();
				Thread.sleep(5000);
				return new ChildrenBenefitRiderAction();
			} catch (Exception e) {
				e.printStackTrace();
				return new ChildrenBenefitRiderAction();
			}
		}else {
			return new ChildrenBenefitRiderAction();
		}
	}
		public void enterDOB(String dob)
		{	
			String[] dateOfBirth = dob.split("/");
			String strMonth = dateOfBirth[0];
			String strDay = dateOfBirth[1];
			String strYear = dateOfBirth[2];
			txtMonth.sendKeys(strMonth);
			txtYear.sendKeys(strYear);
			txtDay.sendKeys(strDay);
			
			extentTest.log(LogStatus.PASS, dob + " is entered in DOB");	
		}
	
}
