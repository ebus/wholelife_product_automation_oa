package com.qa.pageActions;

import static com.qa.pageActions.Illustration_RidersPageAction.childbenefitRiderflag_illus;
import static com.qa.pageActions.WholeLifeCoverageRidersAction.childbenefitRiderflag;
//import static com.qa.pageActions.Illustration_PolicyPageAction.childbenefitRider_illustration_flag;
import com.qa.pages.HealthQuestionsChildrenPage;
import com.relevantcodes.extentreports.LogStatus;

public class HealthQuestionsChildrenPageAction extends HealthQuestionsChildrenPage {
	
	public HealthQuestionsChildrenPageAction() {
		super();
	}
	public FamilyHistoryPageAction detailsOfChildrenHealthQuestion() throws InterruptedException {
		if(childbenefitRiderflag == true || childbenefitRiderflag_illus == true ) {
			extentTest.log(LogStatus.INFO, " - Children Health Information Page - ");
			switchToFrame(frameHealthQuestion_child);
			ClickJSElement(rdoQuestionNo_1_no, "Question_1_No");
			ClickJSElement(rdoQuestionNo_2_no, "Question_2_No");
			ClickJSElement(rdoQuestionNo_3_no, "Question_3_No");
			ClickJSElement(rdoQuestionNo_4_no, "Question_4_No");
			ClickJSElement(rdoQuestionNo_5_no, "Question_5_No");
			ClickJSElement(rdoQuestionNo_6_no, "Question_6_No");
			ClickJSElement(rdoQuestionNo_7_no, "Question_7_No");
			ClickJSElement(rdoQuestionNo_8_no, "Question_8_No");
			ClickJSElement(rdoQuestionNo_9_no, "Question_9_No");
			ClickJSElement(rdoQuestionNo_10_no, "Question_10_No");
			ClickJSElement(rdoQuestionNo_11_no, "Question_11_No");
			ClickJSElement(rdoQuestionNo_12_no, "Question_12_No");
			ClickJSElement(rdoQuestionNo_13_no, "Question_13_No");
			ClickJSElement(rdoQuestionNo_14_no, "Question_14_No");
			ClickJSElement(rdoQuestionNo_15_no, "Question_15_No");
			ClickJSElement(rdoQuestionNo_16_no, "Question_16_No");
			ClickJSElement(rdoQuestionNo_17_no, "Question_17_No");
			ClickJSElement(rdoQuestionNo_18_no, "Question_18_No");
			ClickJSElement(rdoQuestionNo_19_no, "Question_19_No");
			takeScreenshot("HealthQuestionsChildrenPage");
			ClickElement(btnNext, "Next button");
			switchToDefault();
			Thread.sleep(5000);
			return new FamilyHistoryPageAction();
		}
			
		else 
			return new FamilyHistoryPageAction();
		
	}

}
