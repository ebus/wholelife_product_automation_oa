package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class Illustration_PolicyPage extends GenericFunction{
	//Initializing the Page Objects:
	public Illustration_PolicyPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameIllustrationPolicy;
	
	@FindBy(css="select#lb_43")
	public WebElement dropdownGender;
	
	@FindBy(css="#cb_90")
	public WebElement cbUseUnisexRates;
	
	@FindBy(css="select#lb_33")
	public WebElement dropdownPrimaryInsuredClass;
	
	@FindBy(css="#btn_34")
	public WebElement btnRating;
	
	@FindBy(id="modalIframe")
	public WebElement frameRatingDetails;
	
	@FindBy(css="input#flda_4")
	public WebElement txtPermanentFlatExtra;
	
	@FindBy(css="input#flda_6")
	public WebElement txtTemporaryFlatExtra;
	
	@FindBy(css="input#flda_8")
	public WebElement txtForYears;
	
	@FindBy(css="#btn_10")
	public WebElement btnSave;
	
	@FindBy(css="select#lb_58")
	public WebElement dropdownInputMethods;
	
	@FindBy(css="input#flda_3")
	public WebElement txtIntialBaseFaceAmount;
	
	@FindBy(css="input#flda_9")
	public WebElement txtPremiumAmount;
	
	@FindBy(css="#lbl_57")
	public WebElement txtInputMethods;
	
	@FindBy(css="select#lb_7")
	public WebElement dropdownDividends;
	
	@FindBy(css="#btn_5")
	public WebElement btnSaveDividendOption;
	
	@FindBy(id="modalIframe")
	public WebElement frameDividendOptionChange;
	
	@FindBy(css="#cb_49")
	public WebElement cbDividendOptionChange;
	
	@FindBy(css="#grdx8_rc_0_0")
	public WebElement txtFromAgeDividendOptionChange;
	
	@FindBy(css="input#flda_52")
	public WebElement txtPercentageDividendScale;
	
	@FindBy(css="input#cb_13")
	public WebElement cbScheduleLoansWithdrawals;
	
	@FindBy(css="select#lb_59")
	public WebElement dropdownWithdrawalOptions;
	
	@FindBy(css="select#lb_21")
	public WebElement dropdownLoanInterest;
	
	@FindBy(css="select#lb_84")
	public WebElement dropdownDistributionSolves;

	@FindBy(css="select#lb_28")
	public WebElement dropdownPaymentMode;

	@FindBy(css="#rdo_155_1")
	public WebElement rdoIsTermConversion_Yes;
	
	@FindBy(css="#rdo_155_2")
	public WebElement rdoIsTermConversion_No;
	
	@FindBy(css="#btn_156")
	public WebElement btnNext;
}
