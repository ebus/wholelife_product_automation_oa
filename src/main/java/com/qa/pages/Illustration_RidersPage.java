package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class Illustration_RidersPage extends GenericFunction{
	//Initializing the Page Objects:
	public Illustration_RidersPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameIllustrationRiders;
	
	@FindBy(css="input#cb_5")
	public WebElement cbWaiverPremiumDisability;
	
	@FindBy(id="modalIframe")
	public WebElement frameWaiverPremiumDisability;
	
	@FindBy(css="select#lb_73")
	public WebElement dropdownCoveredPerson;
	
	@FindBy(css="input[id=flda_72][alt_id='WPCPFirstName']")
	public WebElement txtFirstName_CP;
	
	@FindBy(css="input#flda_71")
	public WebElement txtLastName_CP;
	
	@FindBy(css="select#lb_76")
	public WebElement dropdownGender_CP;
	
	@FindBy(css="select#lb_77")
	public WebElement dropdownClass;
	
	@FindBy(css="#flda_68")
	public WebElement txtDuration;
	
	@FindBy(css="button#btn_21")
	public WebElement btnWaiverOK;
	
	@FindBy (xpath = "//span/input[@class='jq-dte-month hint']")
	public WebElement txtMonth;
	
	@FindBy (xpath = "//input[@class='jq-dte-day hint']")
	public WebElement txtDay;
	
	@FindBy (xpath = "//span/input[@class='jq-dte-year hint']")
	public WebElement txtYear;
	
	@FindBy(css="input#cb_17")
	public WebElement childrenbenefitrider;
	
	@FindBy(css="input#flda_18")
	public WebElement cbrunits;
	
	@FindBy(css="input#flda_47")
	public WebElement cbrduration;
	
	@FindBy(css="input#cb_26")
	public WebElement cbPUARider;
	
	@FindBy (css = "input#flda_44")
	public WebElement txt1035SinglePremium;
	
	@FindBy (css = "select#lb_1")
	public WebElement dropdownMonthReceived;
	
	@FindBy (css = "input#flda_45")
	public WebElement txtNon1035SinglePremium;
	
	@FindBy(css="#btn_55")
	public WebElement btnModalPremiums;
	
	@FindBy(id="modalIframe")
	public WebElement framePUARiderPremiums;
	
	@FindBy(css="button#btn_5")
	public WebElement btnOKPUARiderPremiums;
	
	@FindBy(css="#cb_75")
	public WebElement cbEnhancedBlendedInsuranceRider;
	
	@FindBy(css="select#lb_21")
	public WebElement dropdownEBIRSpecifiedFace;
	
	@FindBy(css="input#flda_10")
	public WebElement txtEBIRAmount;
	
	@FindBy(css="input#flda_19")
	public WebElement txtTargetPersistencyAge; 
	
	@FindBy(css="input#flda_33")
	public WebElement txtDuration_EnhancedBlendedInsRider;
	
	@FindBy(css="button#btn_6")
	public WebElement btnOK_EnhancedBlendedInsRider;
	
	@FindBy(css="#cb_11")
	public WebElement cbSameInsuredTermRider;
	
	@FindBy(css="select[alt_id=SITRPlan]")
	public WebElement dropdownPlan_SameInsTermRider;
	
	@FindBy(css="select[id=lb_6]")
	public WebElement dropdownClass_SameInsTermRider;
	
	@FindBy(css="button#btn_7")
	public WebElement btnOK__SameInsTermRider;
	
	@FindBy(id="cb_22")
	public WebElement cbSurvivorPurchaseOption;
	
	@FindBy(css="#flda_37")
	public WebElement txtFirstName_SurvivorPurchaseOption;
	
	@FindBy(css="#flda_35")
	public WebElement txtLastName_SurvivorPurchaseOption;
	
	@FindBy(css="select#lb_31")
	public WebElement dropdownGender_SurvivorPurchaseOption;
	
	@FindBy(css="select#lb_32")
	public WebElement dropdownClass_SurvivorPurchaseOption;
	
	@FindBy(css="input#flda_33")
	public WebElement txtCoverageAmt_SurvivorPurchaseOption;
	
	@FindBy(css="input#flda_34")
	public WebElement txtDuration_SurvivorPurchaseOption;
	
	@FindBy(css="button#btn_21")
	public WebElement btnOK__SurvivorPurchaseOption;
	
	@FindBy(css="#btnIllustrations")
	public WebElement btnIllustartion;
	
	@FindBy(css="#spanApplication")
	public WebElement tabApplication;
	
	@FindBy(css="#btn_96")
	public WebElement btnNext;

}
