package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class HealthQuestionscoveredpersonPage extends GenericFunction {
	
	public HealthQuestionscoveredpersonPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	//Page Factory - OR:
		@FindBy(id="CossScreenFrame")
		public WebElement frameHealthQuestion_SPO;
		
		@FindBy(id="btn_67")
		public WebElement firstbtn;
		
		@FindBy(id="modalIframe")
		public WebElement framehealthPage;
		
		@FindBy(id="lb_5")
		public WebElement heigh_ft;
		
		@FindBy(id="lb_9")
		public WebElement height_in;
		
		@FindBy(id="flda_10")
		public WebElement weight;
		
		@FindBy(id="rdo_14_2")
		public WebElement proposedInsuredlostorgainedweight_no; 
		
		@FindBy(id="btn_2")
		public WebElement savebtn;
		
		@FindBy(id="rdo_56_1")
		public WebElement rdoQuestionNo_1_yes;
		
		@FindBy(id="rdo_56_2")
		public WebElement rdoQuestionNo_1_no;
		
		@FindBy(id="rdo_57_1")
		public WebElement rdoQuestionNo_2_yes;
		
		@FindBy(id="rdo_57_2")
		public WebElement rdoQuestionNo_2_no;
		
		@FindBy(id="rdo_58_1")
		public WebElement rdoQuestionNo_3_yes;
		
		@FindBy(id="rdo_58_2")
		public WebElement rdoQuestionNo_3_no;
		
		@FindBy(id="rdo_62_1")
		public WebElement rdoQuestionNo_4_yes;
		
		@FindBy(id="rdo_62_2")
		public WebElement rdoQuestionNo_4_no;
		
		@FindBy(id="rdo_59_1")
		public WebElement rdoQuestionNo_5_yes;
		
		@FindBy(id="rdo_59_2")
		public WebElement rdoQuestionNo_5_no;
		
		@FindBy(id="rdo_61_1")
		public WebElement rdoQuestionNo_6_yes;
		
		@FindBy(id="rdo_61_2")
		public WebElement rdoQuestionNo_6_no;
		
		@FindBy(id="rdo_60_1")
		public WebElement rdoQuestionNo_7_yes;
		
		@FindBy(id="rdo_60_2")
		public WebElement rdoQuestionNo_7_no;
		
		@FindBy(id="rdo_63_1")
		public WebElement rdoQuestionNo_8_yes;
		
		@FindBy(id="rdo_63_2")
		public WebElement rdoQuestionNo_8_no;
		
		@FindBy(id="rdo_64_1")
		public WebElement rdoQuestionNo_9_yes;
		
		@FindBy(id="rdo_64_2")
		public WebElement rdoQuestionNo_9_no;
		
		@FindBy(id="rdo_65_1")
		public WebElement rdoQuestionNo_10_yes;
		
		@FindBy(id="rdo_65_2")
		public WebElement rdoQuestionNo_10_no;
		
		@FindBy(id="rdo_66_1")
		public WebElement rdoQuestionNo_11_yes;
		
		@FindBy(id="rdo_66_2")
		public WebElement rdoQuestionNo_11_no;
		
		@FindBy(id="rdo_51_1")
		public WebElement rdoQuestionNo_12_yes;
		
		@FindBy(id="rdo_51_2")
		public WebElement rdoQuestionNo_12_no;
		
		@FindBy(id="rdo_52_1")
		public WebElement rdoQuestionNo_13_yes;
		
		@FindBy(id="rdo_52_2")
		public WebElement rdoQuestionNo_13_no;
		
		@FindBy(id="rdo_53_1")
		public WebElement rdoQuestionNo_14_yes;
		
		@FindBy(id="rdo_53_2")
		public WebElement rdoQuestionNo_14_no;
		
		@FindBy(id="rdo_54_1")
		public WebElement rdoQuestionNo_15_yes;
		
		@FindBy(id="rdo_54_2")
		public WebElement rdoQuestionNo_15_no;
		
		@FindBy(id="rdo_55_1")
		public WebElement rdoQuestionNo_16_yes;
		
		@FindBy(id="rdo_55_2")
		public WebElement rdoQuestionNo_16_no;
		
		@FindBy(id="rdo_49_1")
		public WebElement rdoQuestionNo_17_yes;
		
		@FindBy(id="rdo_49_2")
		public WebElement rdoQuestionNo_17_no;
		
		@FindBy(id="rdo_50_1")
		public WebElement rdoQuestionNo_18_yes;
		
		@FindBy(id="rdo_50_2")
		public WebElement rdoQuestionNo_18_no;	
	
		
		@FindBy(id="btn_11")
		public WebElement btnNext;	

}
