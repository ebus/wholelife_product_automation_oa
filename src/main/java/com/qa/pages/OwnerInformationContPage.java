package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class OwnerInformationContPage extends GenericFunction{
	
	//Initializing the Page Objects:
	public OwnerInformationContPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameOwnerInformationCont;
	
	@FindBy(xpath="(//div[@class='voffset2 col-lg-5 col-md-6 col-sm-8 col-xs-12']/input)[1]")
	public WebElement txtFirstName;
	
	@FindBy(xpath="(//div[@class='voffset2 col-lg-5 col-md-6 col-sm-8 col-xs-12']/input)[2]")
	public WebElement txtLastName;
	
	@FindBy(xpath="(//div[@class='voffset2 col-lg-6 col-md-7 col-sm-10 col-xs-12']/input)[1]")
	public WebElement txtPosition;
	
	@FindBy(xpath="//div[@class='voffset2 col-lg-6 col-md-7 col-sm-10 col-xs-12']/select")
	public WebElement dropdownState;
	
	@FindBy(id="flda_53")
	public WebElement txtFirstNameTrustee;
	
	@FindBy(id="flda_55")
	public WebElement txtLastNameTrustee;
	
	@FindBy(id="flda_28")
	public WebElement txtSSN;
	
	@FindBy(id="lb_87")
	public WebElement dropdownTrustInfo;
	
	@FindBy(id="btn_7")
	public WebElement btnNext;
}
