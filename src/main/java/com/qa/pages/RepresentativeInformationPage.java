package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class RepresentativeInformationPage extends GenericFunction {
	//Page Factory - OR:
	@FindBy(id="CossScreenFrame")
	public WebElement frameRepresentativeInformation;
	
	@FindBy(id="lb_7")
	public WebElement dropdownRepresentativeCode;
	
	@FindBy(id="flda_20")
	public WebElement txtName;
	
	@FindBy(id="flda_34")
	public WebElement txtPhoneNumber;
	
	@FindBy(id="lb_2")
	public WebElement dropdownLicenseNumber;
	
	@FindBy(id="rdo_27_1")
	public WebElement rdoAdditionalRepresentatives_Yes;
	
	@FindBy(id="rdo_27_2")
	public WebElement rdoAdditionalRepresentatives_No;
	
	@FindBy(id="modalIframe")
	public WebElement frameAddRepresentative;
	
	@FindBy(id="grdx29_addRowButton")
	public WebElement btnClickHere;
	
	@FindBy(id="flda_8")
	public WebElement txtRepresentativesName;
	
	@FindBy(id="flda_15")
	public WebElement txtRepresentativesPercentage;
	
	@FindBy(id="flda_17")
	public WebElement txtRepresentativesCode;
	
	
	@FindBy(id="rdo_63_2")
	public WebElement rdoQuestion2_No;
	
	@FindBy(id="rdo_73_1")
	public WebElement rdoQuestion3_Yes;
	
	@FindBy(id="rdo_61_1")
	public WebElement rdoQuestion4_Yes;
	
	@FindBy(id="cb_69")
	public WebElement cbPhotoID;
		
	@FindBy(id="rdo_79_2")
	public WebElement rdoShdAppEvaluated_No;
	
	@FindBy(id="btn_85")
	public WebElement btnNext;
	
	@FindBy(css="button#btn_12")
	public WebElement btnAgentSave;
	
	//Initializing the Page Objects:
	public RepresentativeInformationPage() {
		super();
		PageFactory.initElements(driver, this);
	}

}
